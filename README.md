# stack-monitoring

  InfluxDB (time-series database), telegraf (agent), Chronograf (Web UI/Dashboard)

## Telegraf

  To run a docker container:
  `docker run -d -v $PWD/telegraf/telegraf.conf:/etc/telegraf/telegraf.conf:ro telegraf`.
